<?php
require_once("../../../vendor/autoload.php");
use App\City\City;

$objCity = new City();

$objCity->setData($_GET);

$oneData = $objCity->view("obj");

echo "<table>";
echo "<tr>";
echo "<th>ID </th>";
echo "<td>:$oneData->id</td>";
echo "</tr>";

echo "<tr>";
echo "<th>Name</th>";
echo "<td>:$oneData->name</td>";
echo "</tr>";

echo "<tr>";
echo "<th>City</th>";
echo "<td>:$oneData->state</td>";
echo "</tr>";
echo "<tr>";
echo "<th>Country</th>";
echo "<td>:$oneData->country</td>";
echo "</tr>";

echo "</table>";