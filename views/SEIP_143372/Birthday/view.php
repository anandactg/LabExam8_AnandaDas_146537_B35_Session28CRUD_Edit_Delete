<?php
require_once("../../../vendor/autoload.php");
use App\Birthday\Birthday;

$objBirthday = new Birthday();

$objBirthday->setData($_GET);

$oneData = $objBirthday->view("obj");

echo "<table>";
echo "<tr>";
echo "<th>ID </th>";
echo "<td>:$oneData->id</td>";
echo "</tr>";

echo "<tr>";
echo "<th>Name</th>";
echo "<td>:$oneData->name</td>";
echo "</tr>";

echo "<tr>";
echo "<th>Birthday</th>";
echo "<td>:$oneData->birthdate</td>";
echo "</tr>";

echo "</table>";