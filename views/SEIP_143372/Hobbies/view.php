<?php
require_once("../../../vendor/autoload.php");
use App\Hobbies\Hobbies;

$objHobbies = new Hobbies();

$objHobbies->setData($_GET);

$oneData = $objHobbies->view("obj");

echo "<table>";
echo "<tr>";
echo "<th>ID </th>";
echo "<td>:$oneData->id</td>";
echo "</tr>";

echo "<tr>";
echo "<th>Name</th>";
echo "<td>:$oneData->name</td>";
echo "</tr>";

echo "<tr>";
echo "<th>hobbies</th>";
echo "<td>:$oneData->hobbies</td>";
echo "</tr>";

echo "</table>";