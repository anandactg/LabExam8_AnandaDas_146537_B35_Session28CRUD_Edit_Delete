<?php
require_once("../../../vendor/autoload.php");
use App\Email\Email;

$objEmail= new Email();

$objEmail->setData($_GET);

$oneData = $objEmail->view("obj");

echo "<table>";
echo "<tr>";
echo "<th>ID </th>";
echo "<td>:$oneData->id</td>";
echo "</tr>";

echo "<tr>";
echo "<th>Name</th>";
echo "<td>:$oneData->name</td>";
echo "</tr>";

echo "<tr>";
echo "<th>Email</th>";
echo "<td>:$oneData->u_email</td>";
echo "</tr>";

echo "</table>";