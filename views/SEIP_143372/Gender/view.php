<?php
require_once("../../../vendor/autoload.php");
use App\Gender\Gender;

$objGender = new Gender();

$objGender->setData($_GET);

$oneData = $objGender->view("obj");

echo "<table>";
echo "<tr>";
echo "<th>ID </th>";
echo "<td>:$oneData->id</td>";
echo "</tr>";

echo "<tr>";
echo "<th>Name</th>";
echo "<td>:$oneData->name</td>";
echo "</tr>";

echo "<tr>";
echo "<th>Gender</th>";
echo "<td>:$oneData->gender</td>";
echo "</tr>";

echo "</table>";