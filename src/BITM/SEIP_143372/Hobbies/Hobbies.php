<?php

namespace App\Hobbies;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Hobbies extends DB{

    public $id="";
    public $name="";
    public $hobbies="";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }

        if(array_key_exists('hobbies',$data)){
           $arr=$data['hobbies'];
            $this->hobbies=implode(",",$arr);
        }


    }

    public function store(){
        $arrData=array($this->name, $this->hobbies);
        $sql="INSERT INTO hobbies(name,hobbies) VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if ($result)
            Message::message("Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");

        Utility::redirect('create.php');
    }

    public function index1($fetchMode = 'ASSOC')
    {


        $STH = $this->DBH->query("SELECT * from hobbies WHERE is_delete='No'");

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;

    }

    public function view($fetchMode = 'ASSOC')
    {

        $STH = $this->DBH->query('SELECT * from hobbies WHERE id=' . $this->id);
        //echo $STH;

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData = $STH->fetch();
        return $arrOneData;

    }

    public function update()
    {

        $arrData = array($this->name, $this->hobbies);
        $sql = "UPDATE hobbies SET name=?, hobbies=? WHERE id=" . $this->id;//UPDATE `atomic_project_b35`.`book_title` SET `book_name` = 'b1' WHERE `book_title`.`id` = 2
        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');
    }

    public function delete()
    {

        $sql = "DELETE FROM hobbies  WHERE id=" . $this->id;//UPDATE `atomic_project_b35`.`book_title` SET `book_name` = 'b1' WHERE `book_title`.`id` = 2
        $STH = $this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');
    }

    public function trash()
    {


        $sql = "UPDATE hobbies SET is_delete=NOW() WHERE id=" . $this->id;//UPDATE `atomic_project_b35`.`book_title` SET `book_name` = 'b1' WHERE `book_title`.`id` = 2
        $STH = $this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');


    }

}

