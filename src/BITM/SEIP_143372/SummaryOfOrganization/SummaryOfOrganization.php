<?php

namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;



class SummaryOfOrganization extends DB{

    public $id="";
    public $name="";
    public $organization="";

    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('org_name',$data)){
            $this->name=$data['org_name'];
        }

        if(array_key_exists('organization',$data)){
            $this->organization=$data['organization'];
        }

    }

    public function store(){
        $arrData=array($this->name, $this->organization);
        $sql="INSERT INTO summary_of_organization (name,organization) VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if ($result)
            Message::message("Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");

        Utility::redirect('index.php');
    }
    public function index1($fetchMode = 'ASSOC')
    {


        $STH = $this->DBH->query("SELECT * from summary_of_organization WHERE is_delete='No'");

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;

    }

    public function view($fetchMode = 'ASSOC')
    {

        $STH = $this->DBH->query('SELECT * from summary_of_organization WHERE id=' . $this->id);
        //echo $STH;

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData = $STH->fetch();
        return $arrOneData;

    }

    public function update()
    {

        $arrData = array($this->name, $this->organization);
        $sql = "UPDATE summary_of_organization SET name=?, organization=? WHERE id=" . $this->id;//UPDATE `atomic_project_b35`.`book_title` SET `book_name` = 'b1' WHERE `book_title`.`id` = 2
        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');
    }

    public function delete()
    {

        $sql = "DELETE FROM summary_of_organization  WHERE id=" . $this->id;//UPDATE `atomic_project_b35`.`book_title` SET `book_name` = 'b1' WHERE `book_title`.`id` = 2
        $STH = $this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');
    }

    public function trash()
    {


        $sql = "UPDATE summary_of_organization SET is_delete=NOW() WHERE id=" . $this->id;//UPDATE `atomic_project_b35`.`book_title` SET `book_name` = 'b1' WHERE `book_title`.`id` = 2
        $STH = $this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');


    }


}